gulp.js - task runner usato nello sviluppo web front end

Ha lo scopo di facilitare, velocizzare, automatizzare l'esecuzione di task

Uso estensivo di plugin (4k+)
	https://gulpjs.com/plugins/

Richiede node (e npm, npx)
	https://nodejs.org/en/download/
	node -v

Installazione (globale) gulp CLI
	npm install -g gulp-cli
	gulp -v

Creazione di un progetto node.js in un nuovo folder
	npm init

Va aggiunto gulp al progetto (dev only)
	npm install --save-dev gulp


-- Creazione del gulpfile.js nella root del progetto -- (1)
//
function aTask(cb) {
  cb();
}

exports.default = aTask
// 

Esecuzione del gulpfile da CLI
	gulp

Esecuzione di un gulpfile (non di default)
	gulp -f xyz.js

-- Task -- (2)
Un task gulp è una funzione asincrona
	pubblica - se la funzione è esportata
	privata altrimenti - normalmente usata per composizione

lista dei task disponibili
	gulp --tasks

-- Esecuzione di più task (3) --
un gruppo di task può essere eseguito in serie o in parallelo via metodi gulp series() e parallel()

-- Segnalazione di errore (4) --
Si può passare un oggetto Error alla callback passata al task
	cb(new Error('Something went wrong'));

-- Lavorare con files -- (5)
Metodi gulp src(), dest(), pipe()

src() prende come argomento un glob per leggere file e ritorna uno stream Node
	glob: / separatore, \\ escape, * 0..n char in un segmento, ** 0..n char, ! esclusione dalla regola precedente 
pipe() permette di creare una pipeline di comandi su uno stream
dest() prende come argomento una directory di output in cui copia i file dello stream su cui opera
	simile a symlink() che crea link ai file

funzioni messe a disposizione da plugin gulp permettono di modificare i file

-- Plugin -- (6)

Minify
JS:   npm install --save-dev gulp-uglify
CSS:  npm install --save-dev gulp-clean-css
HTML: npm install --save-dev gulp-minify-html

