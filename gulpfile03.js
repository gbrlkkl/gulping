// series and parallel
const { series, parallel } = require('gulp');

function a(cb) {
	cb();
}


function b(cb) {
	cb();
}

exports.par = parallel(a, b);
exports.default = series(a, b);