const { src, dest } = require('gulp');

function backupper() {
	return src('original/docs/*.txt').pipe(dest('backup/'));
}

exports.default = backupper;