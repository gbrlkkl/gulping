// public and private tasks
// series of tasks
const { series } = require('gulp');

function pub(cb) {
	cb();
}


function priv(cb) {
	cb();
}

exports.pub = pub;
exports.default = series(pub, priv);